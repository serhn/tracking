<?php

use Illuminate\Database\Seeder;
use App\Country;
use App\City;

class AddCountriesCities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $pathFileCsv = database_path() . "/geo_new.csv";
        if ($fh = fopen($pathFileCsv, 'r')) {
            while (!feof($fh)) {
                $line = fgets($fh);
                $filds = explode(";", str_replace('"','',$line));
                //print_r($filds);
                if($filds[0]=="id"){
                    continue;
                }
                if (!isset($filds[4])) {
                    continue;
                }
                $csvCountry = $filds[4];
                $csvCity = $filds[6];

                $csvLat = $filds[7];
                $csvLng = $filds[8];

                $country = Country::where("country","=", $csvCountry)->first();
                if (!$country) {
                    $country = new Country;
                    $country->country = $csvCountry;
                    $country->save();
                }
                echo $country->id." ".$country->country."\n";

                $city = new City;
                $city->country_id=$country->id;
                $city->city=$csvCity;
                $city->lat=$csvLat;
                $city->lng=$csvLng;
                $city->save();
                echo $city->id." ".$city->city."\n";
            }
            fclose($fh);
        }
    }
}
