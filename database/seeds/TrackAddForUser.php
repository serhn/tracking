<?php

use Illuminate\Database\Seeder;
use App\Coordinate;
use Carbon\Carbon;
use Faker\Generator as Faker;
use App\User;

class TrackAddForUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {

        $i = 1;
        while ($i <= 5) {
            
            $user = new User;
            $user->id = $i;
            $user->city_id = 6482;
            $user->name = $faker->name;
            $user->tel = $faker->phoneNumber;
            $user->email = $faker->unique()->safeEmail;
            $user->password = bcrypt("qwerty");
            $user->save();

            $pathFileCsv = database_path() . "/" . $i . ".csv";
            $sec = 0;
            $plusSec = 5;
            if ($fh = fopen($pathFileCsv, 'r')) {
                while (!feof($fh)) {
                    $line = fgets($fh);
                    $filds = explode(",", str_replace('"', '', $line));
                    if ($filds[0] == "Latitude") {
                        continue;
                    }
                    if (!isset($filds[1])) {
                        continue;
                    }
                    $sec = $sec + $plusSec;

                    $coordinate = new Coordinate;
                    $coordinate->lat = $filds[0];
                    $coordinate->lng = $filds[1];
                    $coordinate->user_id = $i;
                    $coordinate->created_at = Carbon::now()->second($sec);
                    $coordinate->save();

                    echo $i . "\n";
                    print_r($filds);
                }
            }
            $i++;
        }
    }
}
