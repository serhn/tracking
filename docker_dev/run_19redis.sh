#!/bin/sh
. inc/init.sh
IMAGE=redis



docker run  -d \
            --restart unless-stopped  \
            --name="redis-cache" \
            -p 6379:6379 \
            $IMAGE 
