#!/bin/sh
. inc/init.sh
IMAGE="schickling/mailcatcher"
WWW_PORT="1080"
docker run -d \
           -p 1025:1025 \
           -p $WWW_PORT:1080 \
          --name $CONTAINER \
       --restart unless-stopped \
       $IMAGE

echo "${BLUE}http://localhost:$WWW_PORT${NC}  $CONTAINER"
