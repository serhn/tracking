#!/bin/sh
. inc/init.sh
IMAGE=serh/docker-php72dev

docker run  -d \
            --restart unless-stopped  \
            --name="$CONTAINER" \
            --link mysql57:mysql \
            --link mailcatcher \
            --link redis-cache \
            -v ${LARAVEL_DIR}:/usr/share/nginx \
            -v ${PWD}/inc/php.ini:/usr/local/etc/php/php.ini \
            $IMAGE
