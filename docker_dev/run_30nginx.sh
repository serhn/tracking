#!/bin/sh
. inc/init.sh
IMAGE="nginx"
docker run -it -d \
	--restart unless-stopped \
	--link php72dev \
	--name="$CONTAINER" \
        -v ${LARAVEL_DIR}:/usr/share/nginx \
        -v ${PWD}/inc/nginx.conf:/etc/nginx/conf.d/default.conf \
	-p 8880:80 $IMAGE
