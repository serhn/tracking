# API 

GET /api/users - list users with last coordinates

GET /api/user/:user_id - info about user

GET /api/usertrack/:user_id - info about user track

GET /api/countries - countries list

GET /api/country/:country_id  - cities list of country