<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/users', "HomeController@users");
Route::get('/user/{user_id}', "HomeController@user");
Route::get('/usertrack/{user_id}', "HomeController@usertrack");
Route::get('/countries', "HomeController@countries");
Route::get('/country/{country_id}', "HomeController@country");
Route::get('/city/{city_id}', "HomeController@city");