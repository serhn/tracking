import Vue from 'vue'
import Vuex from 'vuex'
//import 'es6-promise/auto'
//import 'performance-polyfill'  // add function performance for old IOS device

import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)
export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    city: { lat: 0, lng: 0 },
    users: {},
    user: {},
    userTrack: {}
  },
  mutations: {
    clickCity(state, city) {
      state.city = city
    },
    clickUser(state, user) {
      state.user = user
    },
    clickUserTrack(state, userTrack) {
      state.userTrack = userTrack
    },
    initUsers(state, users) {
      state.users = users
    }

  },
  actions: {
    clickUser({ commit, state }, user_id) {
      axios
        .get("/api/user/" + user_id)
        .then(response => {
          var user = response.data
          commit("clickUser", user)
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    clickUserTrack({ commit, state }, user_id) {
      axios
        .get("/api/usertrack/" + user_id)
        .then(response => {
          var userTrack = response.data.map(function (item) {
            return {
              lat: Number(item.lat),
              lng: Number(item.lng)
            };
          });
          commit("clickUserTrack", userTrack)
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  }
})
