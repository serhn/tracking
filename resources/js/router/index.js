import Vue from 'vue'
import VueRouter from 'vue-router'
//import Hello from '@/components/Hello'
import MapUsers from '../components/MapUsers'
import MapUser from '../components/MapUser'
import MapCity from '../components/MapCity'
//import UserTrack from '../components/UserTrack'
import CountriesList from '../components/CountriesList'
import CountryCity from '../components/CountryCity'

import UsersList from '../components/UsersList'


Vue.use(VueRouter)
export default new VueRouter({
  mode: 'history',
  linkActiveClass: "active",
  routes: [
    {
      path: "/users",
      alias: "/",
      components: {
        leftColum: UsersList,
        map: MapUsers,
      },
      name: "users"
    },
    {
      path: "/user/:user_id",
      components: {
        leftColum: UsersList,
        map: MapUser,
      },
      name: "userTrack"
    },
    {
      path: "/countries",
      components: { leftColum: CountriesList },
      name: "countriesList"
    },
    {
      path: "/country/:country_id",
      components: { leftColum: CountryCity },
      name: "countryCity"
    },
    {
      path: "/city/:city_id",
      components: {
        map: MapCity,
        leftColum: CountryCity
      },
      name: "cityMap"
    },
  ]
})