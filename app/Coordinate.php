<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{
    protected $hidden = [
         'updated_at'
    ];
    
}
