<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Country;
use App\City;
use App\Coordinate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function users(){
        return User::with(['lastCoordinate'])->get();
    }
    public function user($user_id){
        
        return User::with('city','city.country')->find($user_id);
    }
    public function usertrack($user_id){
        return Coordinate::where('user_id',$user_id)->get();
    }
    public function countries(){
        return Country::orderBy("country")->get();
    }
    public function country($country_id){
        return City::where('country_id',$country_id)->orderBy('city')->get();
    }
    public function city($city_id){
        return City::find($city_id)->first();
    }
}
